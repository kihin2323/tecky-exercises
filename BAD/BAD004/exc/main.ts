import Knex from 'knex'
import configs from './knexfile';
import xlsx from 'xlsx';
import dotenv from 'dotenv';
dotenv.config()
const knex = Knex(configs[process.env.NODE_ENV || "development"])