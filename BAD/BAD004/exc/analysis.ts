import Knex from 'knex';
let config = require('./knexfile');
let knex = Knex(config.development);

export async function q1() {
  let res = await knex
    .select(['user_id', 'username'])
    .count('files.id')
    .from('files')
    .groupBy(['user_id', 'username'])
    .innerJoin('users', 'users.id', 'files.user_id');
  console.log(res);
}

export async function q2() {
  let res = await knex
    .select(['category_id', 'categories.name'])
    .count('files.id')
    .from('files')
    .groupBy(['category_id', 'categories.name'])
    .innerJoin('categories', 'categories.id', 'files.category_id');
  console.log(res);
}

export async function q3() {
  let res = await knex
    .select(['username', 'categories.name'])
    .count('files.id')
    .from('files')
    .groupBy(['categories.name', 'username'])
    .innerJoin('users', 'users.id', 'files.user_id')
    .innerJoin('categories', 'categories.id', 'files.category_id')
    .where({ username: 'alex' })
    .andWhere('categories.name', 'Important');
  console.log(res);
}

export async function q4() {
  let sql = knex
    .select(['owner_id', 'username'])
    .count('files.id')
    .from('files')
    .groupBy(['user_id', 'username'])
    .innerJoin('users', 'users.id', 'files.owner_id')
    .havingRaw('count(files.id) > 800')
    .toSQL().sql;

  let res = await knex.raw(
    `select count (counts.user_id) from (${sql}) as counts`,
  );
  let count = res.rows[0].count;

  console.log('count:', count);
}

async function main() {
  try {
    await q4();
  } catch (error) {
    console.error('failed to query:', error);
  }
  knex.destroy();
}
main();
