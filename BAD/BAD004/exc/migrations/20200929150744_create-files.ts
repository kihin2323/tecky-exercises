import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("files");
    if(!hasTable) {
        return knex.schema.createTable("files", (table)=>{
            table.increments();
            table.string("name",255).notNullable()
            table.string("content",1000).nullable()
            table.boolean("is_file")
            table.integer("category_id")
            table.foreign("category_id").references("categories.id")
            table.integer("owner_id")
            table.foreign("owner_id").references("users.id")
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("files");
}