import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("categories");
    if(!hasTable) {
        return knex.schema.createTable("categories", (table)=>{
            table.increments();
            table.string('name').notNullable()
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("categories");
}

