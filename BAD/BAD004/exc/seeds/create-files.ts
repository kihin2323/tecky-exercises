import * as Knex from "knex";
import xlsx from "xlsx";
import {hashPassword} from "../hash"

async function getUserId(knex: Knex, username: string) {
    let user = await knex
    .select('id')
    .from('users')
    .where({ username })
    .first();
    if (user) {
        return user.id;
    }
    console.error(`failed to get user id of "${username}"`);
    throw new Error(`user not found`);
}

function fixUsername(username: string): string {
    switch (username) {
    case 'gordan':
        return 'gordon';
    case 'alexs':
    case 'ales':
        return 'alex';
    case 'admiin':
        return 'admin';
    case 'micheal':
        return 'michael';
    default:
        return username;
    }
}


export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("files").del();
    await knex("users").del();
    await knex("categories").del();

    const workbook = xlsx.readFile('./BAD004-exercise.xlsx')

    // Inserts seed category
    for (const category of xlsx.utils.sheet_to_json(workbook.Sheets['category']) as any []){
        await knex("categories").insert([
            { name: category.name}
        ]);
    }

    // Inserts seed users
    for (const user of xlsx.utils.sheet_to_json(workbook.Sheets['user']) as any []){
        let hashedPassword = await hashPassword(user.password.toString())
        await knex("users").insert([
            { username: user.username, password: hashedPassword, level: user.level}
        ]);
    }
    // Inserts seed files
    for (const file of xlsx.utils.sheet_to_json(workbook.Sheets['file']) as any []){
        let fileBoolean = file.is_file == 1;
        let context = file.Content ? file.Content : null
        let categoryId = (
            await knex
                .select('id')
                .from('categories')
                .where({ name: file.category })
                .first()
            ).id;
        let ownerName = fixUsername(file.owner)
        let ownerId = await getUserId(knex, ownerName)
        await knex("files").insert([
            { 
                name: file.name, 
                content: context, 
                is_file: fileBoolean,
                category_id: categoryId, 
                owner_id: ownerId,
            }
        ]);
    }
};