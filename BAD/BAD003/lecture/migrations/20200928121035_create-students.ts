import * as Knex from "knex";

export async function up(knex: Knex)  {
    const hasTable = await knex.schema.hasTable("students");
    if(!hasTable){
        return knex.schema.createTable("students",(table)=>{
            table.increments();
            table.string("name");
            table.string("level");
            table.date("date_of_birth");
            table.integer("teacher_id").unsigned();
            table.foreign('teacher_id').references('teachers.id');
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
};

export async function down(knex: Knex){
    return knex.schema.dropTableIfExists("students");
};