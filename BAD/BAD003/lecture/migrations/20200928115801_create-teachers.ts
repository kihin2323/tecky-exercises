import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('teachers');
    if(!hasTable){
        return knex.schema.createTable('teachers',(table)=>{
            table.increments();  //default is id
            table.string("name");   
            table.date("date_of_birth");   
            table.timestamps(false,true);   
        });  
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropSchemaIfExists('teachers')
}

