import {MemoService} from "./memo.service"
import express from "express";
import socketIO from 'socket.io';

class MemoController {

    constructor(
            public memoService:MemoService, 
            public io: socketIO.Server
        ){}

    //method
    get = async(req: express.Request, res: express.Response) => {
        try {
            let memos = await this.memoService.loadAllMemos();
            res.json(memos)
        }catch (error) {
            res.status(500).json(error.toString());
        }
    }

    post = async(req: express.Request, res: express.Response) => {
        try {
            let memo = req.body['memo-content']
            if (!memo || typeof memo !="string" || memo.length == 0){
                return res.status(400).json('invalid memo-content');
                }
            let image = req.file?.filename
            await this.memoService.saveNewMemo({memo,image});
            this.io.emit('create-memo','Create memo successfully!');
            return res.redirect('/');
        }catch(error){
            return res.status(500).json(error.toString());
        }   
    }

}

export default MemoController

// let controller = new MemoController()

// function testMethod(method){
//     method()
// }

// testMethod(controller.post)
