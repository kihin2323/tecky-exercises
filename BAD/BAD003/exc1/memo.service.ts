import Knex from "knex";
import { Memo } from "./models";

export class MemoService {
    constructor(public knex: Knex) {}

    async loadAllMemos() {
    const memos: Memo[] = (
        await this.knex.select('*').from('memos').orderBy('id')
    )
    return memos;
    }

    async saveNewMemo(memo: Memo) {
        const ids = (
                await this.knex.insert({
                    content: memo.memo, image: memo.image
                }).into('memos').returning('id')
        )
        if (ids.length != 1) {
        throw new Error("failed to insert the memo");
        }
        return ids[0];
    }

    async updateMemo (id: number, memo: Memo) {
        const ids = (
            await this.knex.update({
              content: memo.memo, image: memo.image
            }).into('memos')
            .where('id', id)
            .returning('id')
        )
        return ids;
    }

    async deleteMemo(id:number){
        return await this.knex('memos').where('id',id).del();
    }
}