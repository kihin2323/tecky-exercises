import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("memos");
    if(!hasTable){
        return knex.schema.createTable("memos",table=>{
            table.increments();
            table.string("content", 255).notNullable;
            table.string("image", 255).nullable;
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("memos");
}


