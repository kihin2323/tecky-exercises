import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("users")
    if(!hasTable){
        return knex.schema.createTable("users",table=>{
            table.increments();
            table.string("username", 255).notNullable;
            table.string("password", 60).notNullable;
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropSchemaIfExists("users");
}

