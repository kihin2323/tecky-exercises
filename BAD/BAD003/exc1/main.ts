import express from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser'
import multer from 'multer';
import http from 'http';
import socketIO from 'socket.io';
// import {Client} from 'pg';
import dotenv from 'dotenv';
import Knex from 'knex';
import config from "./knexfile";

export const knex = Knex(config[process.env.NODE_ENV || "development"])

dotenv.config();
// export const client = new Client({
//     database: process.env.DB_NAME,
//     user: process.env.DB_USERNAME,
//     password: process.env.DB_PASSWORD,
//     host: process.env.DB_HOST,
// });

// client.connect(); // 只connect 一次，唔洗end

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
    cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
export const upload = multer({storage: storage})


const app = express();
const server = new http.Server(app);
export const io = socketIO(server);

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.use((req,res,next)=>{
    // req, 來信寫咗啲咩
    // res 回信你想寫咩
    if (req.session) {
        if (req.session.count) {
            req.session.count += 1
        } else {
            req.session.count = 1
        }
    }
    console.log("我是A字膊! " + req.path);
    // res.end('I am responsible');
    next();
})

const sessionMiddleware = expressSession({
    secret:"Tecky Academy teaches JavaScript",
    resave:true,
    saveUninitialized:true
})

app.use(sessionMiddleware);

io.use((socket,next)=>{
    sessionMiddleware(socket.request, socket.request.res,next);
})

import grant from 'grant-express';

app.use(grant({
    "defaults":{
        "protocol": "http",
        "host": "localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile","email"],
        "callback": "/login/google"
      },
}));


app.use(express.static('public'));

app.use(express.static('uploads'));

import { memoRoutes } from './memoRoutes';
import { userRoutes } from './userRoutes';
app.use('/',memoRoutes);

app.use('/', userRoutes)

// Create: POST 
// Read: GET
// Update: PUT/PATCH
// Delete: DELETE


app.use((req, res) => {
    console.log(req.session?.user?.username + ' cannot find page') 
    res.json('404')
})

server.listen(8080,()=>{
    console.log("Development Mode started at http://127.0.0.1:8080");
});
// socket 嘅意思係，現家駁緊上嚟果條socket ，所以其實係次次都唔同
io.on('connection',(socket)=>{
    console.log(socket.id);

    if(socket.request.session.user){
        console.log(socket.request.session.user);
        socket.join(`admin`); // user-gordon@tecky.io
    }

    socket.on('haha',(data)=>{
        // 無人規定你必須答嘢嘅
        socket.emit('hihi',{name:"Gordon"}) // 比較少用，因為好難搵得返邊一個socket
    })
});

// setInterval(function(){
//     io.emit("1-second","1 second passed");
// },1000);