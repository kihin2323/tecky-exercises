#%%
## Import the necessary python packages
import os
import matplotlib.pyplot as plt
import tensorflow as tf

print("TensorFlow version: {}".format(tf.__version__))
print("Eager execution: {}".format(tf.executing_eagerly()))


# %%
## loading train_dataset
train_dataset_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_training.csv"
train_dataset_fp = tf.keras.utils.get_file(fname=os.path.basename(train_dataset_url),origin=train_dataset_url)
print("Local copy of the dataset file: {}".format(train_dataset_fp))

# %%
## loading test dataset
test_dataset_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_test.csv"
test_dataset_fp = tf.keras.utils.get_file(fname=os.path.basename(test_dataset_url),
                origin=test_dataset_url)
print("Local copy of the test dataset file: {}".format(test_dataset_fp))

# %%
## columns name of the train_dataset
column_names = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'species']

feature_names = column_names[:-1]
label_name = column_names[-1]

print("Features: {}".format(feature_names))
print("Label: {}".format(label_name))
# %%
## Reading the datasets from the csv
batch_size = 32
raw_train_dataset = tf.data.experimental.make_csv_dataset(
            train_dataset_fp,
            batch_size,
            column_names=column_names,
            label_name=label_name,
            num_epochs=1)
# %%
## Use matplotlib to visualize the dataset we get.
raw_features, raw_labels = next(iter(raw_train_dataset))
print(raw_features)
plt.scatter(raw_features['petal_length'],
    raw_features['sepal_length'],
    c=raw_labels,
    cmap='viridis')

plt.xlabel("Petal length")
plt.ylabel("Sepal length")
plt.show()
# %%
## A helper function to turn the datasets into array
def  pack_features_vector(features, labels):
    """Pack the features into a single array."""
    features = tf.stack(list(features.values()), axis=1)
    return features, labels

# %%
## Extract the features and labels
train_dataset = raw_train_dataset.map(pack_features_vector)
features, labels = next(iter(train_dataset))

print(features)
# %%
raw_test_dataset = tf.data.experimental.make_csv_dataset(
    test_dataset_fp,
    batch_size,
    column_names=column_names,
    label_name='species',
    num_epochs=1,
    shuffle=False)

test_dataset = raw_test_dataset.map(pack_features_vector)
# %%
