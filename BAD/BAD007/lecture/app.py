import tornado.ioloop
import tornado.web
import tensorflow as tf
import numpy as np
import json

class_names = ['Iris setosa', 'Iris versicolor', 'Iris virginica']

class MainHandler(tornado.web.RequestHandler):
    def post(self):
        content = json.loads(self.request.body)
        predict_dataset = tf.convert_to_tensor(content)
        model = tf.saved_model.load('./')
        predictions = model(predict_dataset, training=False)
        results = []
        for i, logits in enumerate(predictions):
            class_idx = tf.argmax(logits).numpy()
            p = tf.nn.softmax(logits)[class_idx]
            name = class_names[class_idx]
            results.append({"name": name,"probability": float(p)})
        self.write({"data":results})

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    server = tornado.httpserver.HTTPServer(app)
    server.bind(8888)
    server.start(2)
    tornado.ioloop.IOLoop.current().start()