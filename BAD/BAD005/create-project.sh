read -p "project name" name
mkdir $name
cd $name
yarn init --yes
yarn add --dev typescript ts-node ts-node-dev
yarn add --dev jest @types/jest ts-jest
yarn add knex dotenv
yarn add --dev @types/knex @types/dotenv
yarn ts-jest config:init
yarn knex init -x ts
cp /Desktop/data/tsconfig.json .