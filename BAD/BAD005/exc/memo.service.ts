import Knex from 'knex';
import { Memo } from './models';

class MemoService {
  constructor(public knex: Knex) {}

  async loadAllMemos() {
    // const memos: Memo[] = (
    //   await this.client.query('SELECT * FROM memos ORDER BY id')
    // ).rows;
    const memos = await this.knex
      .select('*')
      .from('memos')
      .orderBy('id');
    return memos;
  }

  async saveNewMemo(memo: Memo) {
    // const ids = (
    //   await this.client.query(
    //     /*sql*/ `INSERT INTO memos (content,image,created_at,updated_at)
    //         VALUES ($1,$2,NOW(),NOW()) RETURNING id`,
    //     [memo.memo, memo.image],
    //   )
    // ).rows;

    // const ids = await this.knex
    //   .insert({ content: memo.memo, image: memo.image })
    //   .into('memos')
    //   .returning('id');

    const ids = await this.knex('memos')
      .insert({ content: memo.memo, image: memo.image })
      .returning('id');

    if (ids.length != 1) {
      throw new Error('failed to insert the memo');
    }
    return ids[0];
  }
}
export default MemoService;
