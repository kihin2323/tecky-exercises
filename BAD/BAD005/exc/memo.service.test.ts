import MemoService from './memo.service';
import Knex from 'knex';
const knexConfig = require('./knexfile');

// console.log(knexConfig);
// console.log('env:', process.env.NODE_ENV);

let knex: Knex;

beforeAll(() => {
  knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);
});

afterAll(() => {
  knex.destroy();
});

it('should save a new memo', async () => {
  let memoService = new MemoService(knex);
  expect(memoService).toBeDefined();

  let memos = await memoService.loadAllMemos();
  expect(Array.isArray(memos));

  let initialMemoCount = memos.length;

  let newId = await memoService.saveNewMemo({
    memo: 'testing from jest',
  });
  let newMemos = await knex('memos')
    .where('id', newId)
    .select('content');
  expect(newMemos).toHaveLength(1);
  expect(newMemos[0].content).toBe('testing from jest');

  memos = await memoService.loadAllMemos();
  expect(Array.isArray(memos));

  let newMemoCount = memos.length;

  expect(newMemoCount).toBe(initialMemoCount + 1);
});
