import * as Knex from 'knex';
import users from '../users.json';
import memos from '../memos.json';
import { hashPassword } from '../hash';

// console.log('users:', users);

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex('memos').del();
  await knex('users').del();

  // Inserts seed entries

  for (let user of users) {
    let hashedPassword = await hashPassword(user.password);
    await knex
      .insert({ username: user.username, password: hashedPassword })
      .into('users');
  }

  await knex
    .insert(
      memos.map(memo => ({
        content: memo.memo,
        image: memo.image,
      })),
    )
    .into('memos');
}
