import Knex from 'knex';
import knexConfigs from './knexfile';
import { UserService } from './user.service';
describe('users service test', ()=> {
    let knex: Knex
    let userService: UserService

    beforeEach(async()=>{
        knex = Knex(knexConfigs.active)
        await knex('users').del();
        await knex('users').insert({
            username:"Alex",
            password:"tecky"
        }).into('users')
        userService = new UserService(knex)
    });

    afterAll(()=>{
        knex.destroy();
    })

    it('should get all users', async ()=> {
        const users = await userService.getUsers();
        expect(users).toHaveLength(1);
    })

})