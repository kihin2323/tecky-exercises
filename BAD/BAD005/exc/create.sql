create table users (
    id SERIAL primary key,
    username varchar(255),
    password varchar(60)
);

create table memos (
    id SERIAL primary key,
    content text,
    image text,
    updated_at timestamp,
    created_at timestamp
);

alter table memos
add column
created_at timestamp;