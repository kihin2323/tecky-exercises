import Knex from 'knex'
export class UserService {
    constructor( public knex: Knex){}
    
    async getUsers(){
        let users = this.knex.select('*').from('users');
        return users;
    }
}