import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('memos')){
        return
    }
    await knex.schema.createTable('memos',table=>{
        table.increments()
        table.text('content')
        table.text('image')
        table.timestamps(false, true)
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('memos')
}

