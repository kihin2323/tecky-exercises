import sum from "./sum";

let result = sum(1, 3)

test('1 + 2 = 3', ()=> {
    expect(sum(1, 2)).toBe(3);
})

test('2 + 2 = 3', ()=> {
    expect(sum(2, 2)).toBe(4);
})
