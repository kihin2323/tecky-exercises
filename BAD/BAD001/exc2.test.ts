function fizzBizz (num) {
    let arr = []
    for (let i = 1; i <= num ; i++){

        if(i % 3 == 0 && i % 5 == 0){
            arr.push('fizzBuzz')
        }else if(i % 3 == 0){
            arr.push('fizz')
        }
        else if(i % 5 == 0){
            arr.push('Buzz')
        }else{arr.push(i)}
    }
    return arr;
}

fizzBizz(20)

test('fizzBizz', () => {
    let length = 150
    let result = fizzBizz(length)
    expect(result[length - 1]).toBe('fizzBuzz')
})