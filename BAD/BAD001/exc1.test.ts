function factorial(num:number):number{
    if(num == 0 || num == 1){
        return 1;
    }
    return factorial(num -1 )* num
}

function fibonacci(num:number):number{ //3
    if(num == 1 || num == 2){
        return 1;
    }
    return fibonacci(num - 1)+ fibonacci(num - 2)
}

test('factorial', ()=> {
    expect(factorial(3)).toBe(6)
})

test('fibonacci', () => {
    expect(fibonacci(3)).toBe(2)
})