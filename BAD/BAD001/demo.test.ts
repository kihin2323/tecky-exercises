test("equal test", () => {
  expect(1).toBe(1);
  expect({ user: 1 }).toEqual({ user: 1 });
});

test("compare test", () => {
  expect(5).toBeGreaterThan(4);
  expect(6).toBeGreaterThan(4);
});

test("string", () => {
  expect("team").toMatch("a");
  expect("team").toMatch(/a/); //regular expression
  expect("team").toMatch(/a/);
});

const shoppingList = [
  "diapers",
  "kleenex",
  "trash bags",
  "paper towels",
  "beer",
];

test("the shopping list has beer on it", () => {
  expect(shoppingList).toContain("beer");
});
class ConfigError extends Error {}
function doSomething() {
    throw new ConfigError('i will fail');
}
test('test exception', () => {
    expect(doSomething).toThrowError();
})