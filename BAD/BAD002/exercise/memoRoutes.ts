import express,{Request,Response} from 'express';
import {client, upload, io} from './main';
import { Memo } from './models';
import { isLoggedIn } from './guard';

export const memoRoutes = express.Router();


memoRoutes.get('/memos',getMemos);
memoRoutes.post('/memos', upload.single('image'),postMemo )
memoRoutes.put('/memos/:id', isLoggedIn, upload.single('image'), putMemo );
memoRoutes.delete('/memos/:id',isLoggedIn,deleteMemo);

export async function getMemos(req:Request, res:Response){
    const memos:Memo[] = (await client.query('SELECT * FROM memos ORDER BY id')).rows;
    res.json(memos);
}

export async function postMemo(req:Request, res:Response){

    const ids =  (await client.query(/*sql*/`INSERT INTO memos (content,image,created_at,updated_at) 
            VALUES ($1,$2,NOW(),NOW()) RETURNING id`,[req.body['memo-content'],req.file?.filename])).rows;
    console.log(ids);
    io.emit('create-memo','Create memo successfully!');
    res.redirect('/');
}


export async function putMemo(req:Request,res:Response){
    await client.query(`UPDATE memos SET content = $1, image = $2, updated_at = NOW() WHERE id = $3`,
            [req.body.content,req.file?.filename,req.params.id]);
    res.json({result: 'ok'})
}

export async function deleteMemo(req:Request,res:Response){
    await client.query(`DELETE FROM memos where id = $1`,[req.params.id]);
    res.json({result: 'ok'})
}

