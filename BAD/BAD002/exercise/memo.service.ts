import { Client } from "pg";
import { Memo } from "./models";

class MemoService {
	constructor(public client: Client) {}

	async loadAllMemos() {
		const memos: Memo[] = (
			await this.client.query("SELECT * FROM memos ORDER BY id")
		).rows;
		return memos;
	}

	async saveNewMemo(memo: Memo) {
		const ids = (
			await this.client.query(
				/*sql*/ `INSERT INTO memos (content,image,created_at,updated_at) 
                VALUES ($1,$2,NOW(),NOW()) RETURNING id`,
				[memo.memo, memo.image]
			)
		).rows;
		if (ids.length != 1) {
			throw new Error("failed to insert the memo");
		}
		return ids[0];
	}
}

export default MemoService;
