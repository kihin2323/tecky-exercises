export interface Memo{
    memo: string;
    image?: string;
}

export interface User{
    username: string;
    password: string;
}