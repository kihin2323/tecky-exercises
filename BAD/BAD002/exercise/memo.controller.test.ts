import { Client } from "pg"
import MemoController from "./memo.controller"
import MemoService from "./memo.service"
import socketIO from 'socket.io';
import express from 'express';

describe('memo controller testsuit', ()=> {
    let memoController: MemoController
    let memoService: MemoService
    let io: socketIO.Server
    let req: express.Request
    let res: express.Response
    beforeEach(()=> {
        memoService = new MemoService({} as Client)

        jest
            .spyOn(memoService,'loadAllMemos')
            .mockImplementation(async()=>[{memo:'Hello', image:'hello.jpg'}])
            
        jest
            .spyOn(memoService,'saveNewMemo')
            .mockImplementation(async ()=> 2)
        io = {
            emit:jest.fn()
        } as any;
        memoController = new MemoController(memoService, io)
        req= {} as express.Request
        res= {
            json:jest.fn(),
            redirect: jest.fn(),
            status:jest.fn(()=> {
                return{
                    json:(data: any)=>{
                        console.log(data)
                    }
                }
            }),
        } as any;
    })

    it('should load all memos', async()=> {
        await memoController.get(req, res);
        expect(memoService.loadAllMemos).toBeCalled()
        expect(res.json).toBeCalledWith([{memo:'Hello', image:'hello.jpg'}])
    })

    it('should save the memo', async()=>{
        req.body={
            'memo-content':'Hi Hi'
        }
        req.file={
            filename:'earth.jpg'
        } as any
        await memoController.post(req,res)
        expect(memoService.saveNewMemo).toBeCalledWith({
            memo: 'Hi Hi',
            image: 'earth.jpg'
        })
        expect(io.emit).toBeCalledWith('create-memo','Create memo successfully!')
        expect(res.redirect).toBeCalledWith('/')
    })
})