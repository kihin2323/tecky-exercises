#%%
## Import the necessary package
import matplotlib.pyplot as plt
import tensorflow as tf
import sys
import os
## IMPORTANT!! Importing in Python requires to add the current folder to path
sys.path.append(os.getcwd())
from datasets import train_batches,IMG_SIZE,validation_batches,image_batch
# %%
## Create the base model from the pre-trained model MobileNet V2 using our IMG_SHAPE as the input_shape

IMG_SHAPE = (IMG_SIZE, IMG_SIZE, 3)
base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                        include_top=False,
                                        weights='imagenet')
# %%
## Pass our image_batch to the model, the feature_batch is what we get.
feature_batch = base_model(image_batch)
print(feature_batch.shape)
# %%
## Add a global_average_layer to pool the output from `MobileNet` to transfer the knowledge for our problem.
global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
feature_batch_average = global_average_layer(feature_batch)
print(feature_batch_average.shape)

# %%
## Add prediction layer for the actual prediction.
prediction_layer = tf.keras.layers.Dense(1)
prediction_batch = prediction_layer(feature_batch_average)
print(prediction_batch.shape)
# %%
## Combine the base_model and the two newly added layers together.
model = tf.keras.Sequential([
    base_model,
    global_average_layer,
    prediction_layer
])

#%%
## Setup learning rate, optimizer and loss function
base_learning_rate = 0.0001
model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=base_learning_rate),
                loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                metrics=['accuracy'])

## Show the architecture of the combine model.
model.summary()

# %%
## Try to use validation_batches to avoid overfitting.
initial_epochs = 10
validation_steps=20

loss0,accuracy0 = model.evaluate(validation_batches, steps = validation_steps)
print("initial loss: {:.2f}".format(loss0))
print("initial accuracy: {:.2f}".format(accuracy0))

# %%
## The actual training step
history = model.fit(train_batches,
                epochs=initial_epochs,
                validation_data=validation_batches)
# %%
