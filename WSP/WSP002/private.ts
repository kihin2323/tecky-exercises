class Person {
    lastName: String;
    public firstName: string;
    private age: number;
    protected salary: number;

    constructor(birthYear: number) {
        this.age = 2020 - birthYear
    }
    selfIntro() {
        console.log("my age is", this.age)
    }
}

let tony = new Person(1994)
console.log(tony.selfIntro())
// class Employee extends Person {
//     console.log
// }