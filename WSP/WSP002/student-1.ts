class Student{
    name: string
    constructor(name: string){
        this.name = name
    }
    greet() {
        console.log('Hi, i am ', this.name)
    }
    
}

let amy = new Student("Amy");
console.log(amy.name)
amy.greet()