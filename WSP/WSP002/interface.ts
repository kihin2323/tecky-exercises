export interface Student {
    name: string;
    learn(hoursSpent:number):void;
}
export class Teckystudent implements Student {
    name: string;
    learn(hoursSpent: number): void {
        throw new Error("Method not implemented.");
    }

}