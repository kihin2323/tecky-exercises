class StudentClass{
    /*Fields*/
    name:string;
    age:number;
    learningLevel:number; 

    /*Constructor*/
    constructor(name:string,age:number){
        this.name = name;
        this.age = age;
        this.learningLevel = 0;
    }

    /* Methods */
    learn(hourSpent:number){
        this.learningLevel += hourSpent * 0.3 
    }

    slack(hourSpent:number){
        this.learningLevel -= hourSpent * 0.1
    }
}

let david = new StudentClass("Amy",21)
david.learn(5)
console.log(david.learningLevel) //5*0.3

