export interface Person {
    name: string
}
export interface HasMother {
    mother: Person
}

export interface Student extends Person { }

export interface Teacher {
    student: Student
}

export class TeckyTeacher implements Teacher, Person {
    constructor(public name: string,public student: Student) {}
}
export class Teckystudent implements Person, Student {
    constructor(public name: string) {}
}
export function findPerson(persons: Person[], name: string) {
    for (let person of persons) {
        if (person.name === name) {
            return person;
        }
    }
    return undefined
}
let alice = new Teckystudent("Alice")
let bob = new TeckyTeacher("Bob",alice)
let result = findPerson(
    [alice, bob],
    "Alice"
)
console.log(result)