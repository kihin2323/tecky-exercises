export abstract class Animal {
    abstract eat(): void
    abstract sleep(): void
    constructor(public name: string) { }
    selfIntro() {
        console.log("Hi i am", this.name)
    }
}
// let cat = new Animal() //唔用得 abstract class 唔係實體野 似一個殼咁
export class Human extends Animal {
    eat(): void {
        console.log('i am eating meat or vegetable')
    }
    sleep(): void {
        console.log('I am sleep in bedroom')
    }

}
export class Cat extends Animal {
    eat(): void {
        console.log('i am eating fish')
    }
    sleep(): void {
        console.log('I am sleep in cat nest')
    }

}
let alice = new Human('Alice')
alice.selfIntro();
alice.eat();
let kitty = new Cat('Kitty')
kitty.selfIntro();
kitty.eat();

function play(animal: Animal) {
    console.log('I am playing with', animal.name)
}
play(alice)
play(kitty)