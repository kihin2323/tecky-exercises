import pg from 'pg';
import xlsx from 'xlsx';

const client = new pg.Client({
    host: 'localhost',
    user: 'kihin2323',
    password: '123123',
    database: 'tecky'
})
client.connect()
async function main() {
    // const res = await client.query('SELECT $1::text as message', ['Hello world!'])
    const workbook = xlsx.readFile('./WSP009-exercise.xlsx')
    var sheet_name_list = workbook.SheetNames;
    console.log(xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])) //xlsx.sheet.user
    for(const user of xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]) as any[]) {
        await client.query('INSERT INTO "user" (username, password, level) VALUES ($1, $2, $3)', [user.username, user.password, user.level])
    }
    for(const file of xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[1]]) as any[]) {
        await client.query('INSERT INTO "files" (name, context, is_file) VALUES ($1, $2, $3)', [file.name, file.content, file.isFile])
    }
    for(const category of xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[2]]) as any[]) {
        await client.query('INSERT INTO "categories" (name) VALUES ($1)', [category.name])
    }
    await client.end()
}
main();