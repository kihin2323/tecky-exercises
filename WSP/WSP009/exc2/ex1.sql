CREATE TABLE "user"(
    id SERIAL primary key,
    username VARCHAR(255) not null,
    password VARCHAR(255) not null,
    level VARCHAR(255) not null,
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE "files"(
    id SERIAL primary key,
    name VARCHAR(255) not null,
    is_directory BOOLEAN,
    context TEXT,
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE "categories" (
    id SERIAL primary key,
    name VARCHAR(255),
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE "memberships" (
    id SERIAL primary key,
    name VARCHAR(255),
    price NUMERIC(15, 3),
    created TIMESTAMP,
    updated TIMESTAMP
);

