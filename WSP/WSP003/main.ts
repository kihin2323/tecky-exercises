import fs from 'fs'
import path from 'path'

const js = '.js'


function listAllJs(paths : string) {
    fs.readdir(paths, function (err, files) {
        for (let file of files) {
            let fullPath = path.join(paths + file)
            fs.stat(fullPath, function (err, stats) {
                if (err) {
                    console.log(err);
                    return
                } else if (stats.isFile()) {
                    if(path.extname(file) == js) {
                        console.log(fullPath + file);
                    }
                } else if (stats.isDirectory()){
                    listAllJs(paths + "/" + file)
                }
            })
        }
    })
}

listAllJs("/home/kenneth/tecky-exercises/WSP003exc");
// const currentPath = '/home/kenneth/tecky-exercises/WSP003exc/jsfile/from/'
// const newPath = '/home/kenneth/tecky-exercises/WSP003exc/jsfile/to/'

// let files = ['star.js','test.js']; 

// // I am using simple for, you can use any variant here
// for (var i = files.length + 1; i >= 0; i++) {
//     var file = files[i];
//     fs.rename(currentPath + file, newPath + file, function(err) {
//         if (err) throw err;
//         console.log('Move complete.');
//     });
// }