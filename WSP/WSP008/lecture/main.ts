import express from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser'
import fs from 'fs'
import multer from 'multer';

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
const upload = multer({storage: storage})

interface User {
    username: string;
    password: string;
}

const users: User[] = JSON.parse(fs.readFileSync('users.json', 'utf8'))

const app = express();
const memos: {
    memo: string;
    image: string;
}[] = JSON.parse(fs.readFileSync('memos.json','utf8'));

app.use(bodyParser.urlencoded({extended: false}))

app.use((req,res,next)=>{
    // req, 來信寫咗啲咩
    // res 回信你想寫咩
    if (req.session) {
        if (req.session.count) {
            req.session.count += 1
        } else {
            req.session.count = 1
        }
    }
    console.log("我是A字膊! " + req.path);
    // res.end('I am responsible');
    next();
})

app.use(expressSession({
    secret:"Tecky Academy teaches JavaScript",
    resave:true,
    saveUninitialized:true
}));

app.use(express.static('public'));

app.use(express.static('uploads'));

app.get('/memos', async (req, res) => {
    const memos = await fs.promises.readFile('memos.json', 'utf8')
    res.json(JSON.parse(memos))
})

app.post('/memos', upload.single('image'), async (req, res) => {
    memos.push({
        memo: req.body['memo-content'],
        image: req.file?.filename
    })
    await fs.promises.writeFile('memos.json', JSON.stringify(memos))
    res.redirect('./')
})

app.post('/login', (req, res) => {
    const user = users.find(user => user.username == req.body.username)
    if (user != null && req.session) {
        if (user.password == req.body.password) {
            req.session.user = {
                username: user.username
            };
            res.redirect('/admin/admin.html')
            return;
        }
    }
    res.json({result: 'incorrect password or username'})
})

const isLoggedIn = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.session && req.session.user != null) {
        next();
    } else {
        res.status(401).json({result: 'Unauthorized'})
    }
}

// app.get("/admin.html", isLoggedIn, (req, res) => {
//     res.sendFile("/protected/admin.html")
// })

app.use('/admin', isLoggedIn, express.static('protected'))

app.use((req, res) => {
    console.log(req.session?.user?.username + ' cannot find page') 
    res.json('404')
})

app.listen(8080,()=>{
    console.log("Development Mode started at http://127.0.0.1:8080");
});