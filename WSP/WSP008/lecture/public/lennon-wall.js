const memosContainer = document.querySelector('.memos')
async function loadMemos() {
    const res = await fetch('/memos');
    const memos = await res.json()
    memosContainer.innerHTML = '';
    for (let memo of memos) {
        if(memo.image != null) {
            memosContainer.innerHTML += `
                <div class="memo">
                    ${memo.memo}
                    <img src="${memo.image}">
                    <div class="edit-button">
                        <i class="fas fa-edit"></i>
                    </div>
                    <div class="delete-button">
                        <i class="fas fa-trash-alt"></i>
                    </div>
                </div>
                `
        }else {
            memosContainer.innerHTML += `
            <div class="memo">
                ${memo.memo}
                <div class="edit-button">
                    <i class="fas fa-edit"></i>
                </div>
                <div class="delete-button">
                    <i class="fas fa-trash-alt"></i>
                </div>
            </div>
            `
        }
    }
    console.log(memos)
}

const newForm = document.querySelector('#input-new-memo-form')
newForm.addEventListener('submit', async(event) => {
    event.preventDefault();
    const formData = new FormData(newForm)
    await fetch('/memos', {
        method:'POST',
        body:formData
    })
    loadMemos();
})

