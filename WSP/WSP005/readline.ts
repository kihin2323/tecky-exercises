import * as readline from 'readline';

export function readlinePromise(question: string): Promise<string> {
  const readLineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })
  
  return new Promise((resolve, reject) => {
    readLineInterface.question(question,(answer:string)=>{
      readLineInterface.close();
      resolve(answer)
    });
  })
}