const fs = require('fs');

async function readTheReport() {
    // Detail Here
    //Read from JSON file
    const bufferPromise: Promise<Buffer> = fs.promises.readFile('/home/kenneth/tecky-exercises/WSP005/result.json')
    console.log("step 1", bufferPromise)
    const buffer: Buffer = await bufferPromise;
    console.log("step 1.1", buffer)
    const resultString: string = buffer.toString();
    console.log("step 2", resultString)
    const result = JSON.parse(resultString)
    console.log("step 3", result)
    // const result = JSON.parse((await fs.promises.readFile('result.json')).toString());
    // same as upper 4 lines code 

}

readTheReport();