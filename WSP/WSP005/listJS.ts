import fs from 'fs',
import path from 'path'
// import util from 'util'

// const fsReadFilePromise = util.promisify(fs.readFile)

function fsReadDirPromise(searchPath: string): Promise<string[]> {
  return new Promise((resolve, reject) => {
    fs.readdir(searchPath, (err, files) => {
      if (err != null) {
        reject(err);
        return;
      }
      resolve(files);
    })
  })
}

function fsStatPromise(filePath: string): Promise<fs.Stats> {
  return new Promise((resolve, reject) => {
    fs.stat(filePath, (err, stat) => {
      if (err != null) {
        reject(err);
        return;
      }

      resolve(stat)
    })
  });
}

// let arr = [1,2,3]
// let arrB = [4,5,6]

// const arrC = arr.concat(arrB) 
// [1,2,3,4,5,6]

export async function listAllJsRecursive(searchPath: string) {
  const files = await fsReadDirPromise(searchPath);
  let result: string[] = []
  for (const file of files) {
    const fullPath = path.join(searchPath, file)
    const stat = await fsStatPromise(fullPath)
    if (stat.isFile() && path.extname(file) === '.js') {
      result.push(fullPath)
    }
    if (stat.isDirectory()) {
      result = result.concat(await listAllJsRecursive(fullPath))
    }
  }
  return result;
}