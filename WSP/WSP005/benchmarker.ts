import { readlinePromise } from './readline'; // from previous exercise
import { listAllJsRecursive } from './listJS';
import * as os from 'os';
import * as fs from 'fs';
const readCommand = async () => {
    while (true) { // game-loop, eval-loop
        // Exit by Ctrl+C
        const answer = await readlinePromise("Please choose read the report(1) or run the benchmark(2):");
        const option = parseInt(answer, 10);
        console.log(`Option ${answer} chosen.`);
        if (option == 1) {
            await readTheReport();
        } else if (option == 2) {
            await runTheBenchmark();
        } else {
            console.log("Please input 1 or 2 only.");
        }
    }
}

readCommand();

async function runTheBenchmark() {
    const trial1 = await runTest(1);
    const trial100 = await runTest(100);
    const trial1000 = await runTest(1000);
    const result = JSON.parse((await fs.promises.readFile('result.json')).toString())
    result.push(trial1);
    result.push(trial100);
    result.push(trial1000);
    fs.promises.writeFile('result.json', JSON.stringify(result, null, 4));
    //write the result
}

async function runTest(n: number): Promise<Trial> {
    const start = new Date();
    const memBefore = os.freemem();
    for (let i = 0; i < n; i++) {
        await listAllJsRecursive('./')
    }
    const memAfter = os.freemem();
    const end = new Date();

    const trial = {
        startDate: start.toISOString(),
        endDate: end.toISOString(),
        timeNeeded: (end.getTime() - start.getTime()) / 1000,
        extraMenUsed: memBefore - memAfter,
        name: `${n} times`
    }
    return trial;
}

type Report = Trial[]

interface Trial {
    startDate: string;
    endDate: string;
    timeNeeded: number;
    extraMenUsed: number;
    name: string;
    //Think of what fields are necessary
}

async function readTheReport() {
    // Detail Here
    //Read from JSON file
    const bufferPromise: Promise<Buffer> = fs.promises.readFile('result.json')
    console.log(bufferPromise)
    const buffer: Buffer = await bufferPromise;
    const resultString: string = buffer.toString();
    const result: Report = JSON.parse(resultString)

    // const result = JSON.parse((await fs.promises.readFile('result.json')).toString());
    // same as upper 4 lines code 

    for (let trial of result) {
        console.log(`=========${trial.name}=========`)
        console.log(`start Date ${trial.startDate}`)
        console.log(`End Date: ${trial.endDate}`)
        console.log(`Time needed: ${trial.timeNeeded}`);
        console.log(`Extra Memory Used: ${trial.extraMenUsed}`);
        console.log(`                                             `)
    }
}