import * as fs from "fs"
// function fsReadFilePromise(filePath:string):Promise<Buffer>{
//     return new Promise(function(resolve,reject){
//         // Detail Here
//     });
// }

function ReadFilePromise(filePath:string):Promise<void>{
    return new Promise(function(resolve,reject){
        // Detail Here
        fs.readFile(filePath, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
          });
    });
}

console.log(ReadFilePromise("./main.ts"))
// function fsWriteFilePromise(filePath:string,data:any,options:fs.WriteFileOptions):Promise<{}>{
//     return new Promise(function(resolve,reject){
//         // Detail Here
//         fs.writeFile(filePath, data, options, (err) => {
//             if (err) {
//                 reject();
//             }
//             resolve();
//             console.log('The file has been saved!');
//           })
//     });
// }

// fsWriteFilePromise("./abc.txt","data",'utf8').then(()=>
//     {fsWriteFilePromise("./abc.txt","user",'utf8')}
// )