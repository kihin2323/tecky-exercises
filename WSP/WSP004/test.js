var lines;                                 // Added compared to basic code.
const readline = require('readline');
const rl = readline.createInterface({
  input:  process.stdin,
  output: process.stdout
});
rl.question("Which dices to keep [1,2,3,4,5] ?: ", (answer) => {
  lines.push(answer);                      // Added compared to basic code.
  console.log("Will keep dices: ", answer);
  rl.close();
});
console.log(lines);      