import express from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser'
import fs from 'fs';
import multer from 'multer'

const app = express();

const upload = multer({dest:'uploads/'})

app.use(expressSession({
    secret: "Tecky Academy teaches JavaScript",
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended:false}))

app.use(express.static('public'));

let memoArr:any[] = [];

app.post('/saveForm',upload.single('image'),async(req,res,next)=> {
    if(req.body.memo || req.body.image){
        memoArr.push(`content: ${req.body.memo}, image:${req.body.image}`)
        fs.promises.writeFile('memos.json', JSON.stringify(memoArr, null, 4));
    }
    res.redirect('/');
})
app.get('/memos', async(req, res)=> {
    await res.json(fs.promises.readFile('memos.json','utf-8'))
})

app.listen(8080,()=>{
    console.log("Development Mode started at http://127.0.0.1:8080");
});