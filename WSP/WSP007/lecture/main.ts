import express from 'express'
import expressSession from "express-session"
import bodyParser from 'body-parser'

const app = express()

app.use(expressSession({
    secret: "Tecky Academy teaches JavaScript",
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.urlencoded())

app.use(express.static("public"))

app.post('/login', (req,res) => {
    if(req.body.user == 'alex' && req.body.pass == '123123' && req.session){
        req.session.isAdmin = true;
        res.json('歡迎主人')
    }else {
        res.json("who ar u ")
    }
})

app.get('/users', (req, res) => {
    if (req.session && req.session.isAdmin) {
      res.json([{name:'Alex'}, {name:'Gordon'}])
    } else {
      res.status(403).json('Permission denied')
    }
  })

app.get('/', (req, res) => {
    if (req.session) {
        if (req.session.count) {
            req.session.count = req.session.count + 1;
        } else {
            req.session.count = 1
        }
        res.json(`Hi. This is the ${req.session.count} time visit.`)
    } else {
        res.json(`Hi`)
    }
})

// app.use(express.static('public'));

const PORT = 8080

app.listen(PORT, () => {
    console.log(`Listening at port ${PORT}`)
})