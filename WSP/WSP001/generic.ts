//interface
interface NumberList {
    value: number
    tail?: NumberList
}
let numberList: NumberList = {
    value: 1,
    tail: undefined
}

interface StringList {
    value: number
    tail?: StringList
}

//generic type of interface
function increment(num: number): number {
    return num + 1
}

interface List<Item> {
    value: Item;
    tail?: List<Item>
}
let numList: List<number> = {
    value: 1,
    tail: undefined
}

let strlist: List<String> = {
    value: "2",
    tail: undefined
}

// function findNext<Item>(list: List<Item>, item: Item): Item | undefined {
//     for (; ;) {
//         if (list.value === item) {
//             if (list.tail) {
//                 return list.tail.value
//             }
//             return undefined
//         }
//         if(!list.tail) {
//             break;
//         }
//         list = list.tail;
//     }
// }
//same as top function
function findNext<Item>(list: List<Item>, item: Item): Item | undefined {
    for (; ;) {
        if (list.value === item) {
            console.log(item)
            return list.tail?.value;
        }
        if (!list.tail) {
            return undefined;
        }
        list = list.tail;
    }
}
let numResult = findNext({ value: 1, tail: { value: 2 } }, 1)
console.log(numResult);

let strResult = findNext({ value: "a", tail: { value: "b" } }, "a")
console.log(strResult)

function printAllSince<Item>(array: Array<Item>, sinceItem: Item) {
    let seen = false;
    array.forEach((item) => {
        if (item === sinceItem){
            seen = true
        }
        if (seen) {
            console.log(item);
        }
    })
}
let number = [4, 5, 4]
printAllSince(number ,5)
let letter = ["1", "2", "3"]
printAllSince(letter, "2")