const memosContainer = document.querySelector('.memos')

let isAdmin = false

function stripHtml(html)
{
   var tmp = document.createElement("DIV");
   tmp.innerText = html;
   return tmp.innerHTML;
}

async function loadMemos() {
  // GET /memos
  const res = await fetch('/memos')
  const memos = await res.json()

  memosContainer.innerHTML = '';

  let i = 0;
  for (const memo of memos) {
    let memoHTML = `
      <div class="memo">
          <div class="content">${stripHtml(memo.memo)}</div>
    `
    if (memo.image != null) {
      memoHTML += `<img src="${memo.image}">`;
    }
    if (isAdmin) {
      memoHTML += `
      <div class="edit-button" data-teckyid="${i}">
          <i class="fas fa-edit"></i>
      </div>
      <div class="delete-button" data-teckyid="${i}">
          <i class="fas fa-trash-alt"></i>
      </div>
      `
    }
    memoHTML += `
      </div>
    `
    memosContainer.innerHTML += memoHTML;
    i++;
  }
  // 2
  // const deleteButtons = document.querySelectorAll('.delete-button')
  // for (const deleteButton of deleteButtons) {
  //   deleteButton.addEventListener('click', async () => {
  //     await fetch('/memos/' + deleteButton.dataset.teckyid, {
  //       method: 'DELETE'
  //     })

  //     loadMemos();
  //   })
  // }
  console.log(memos)
}
// 1
memosContainer.addEventListener('click', async (event) => {
  if (event.target.matches('.delete-button')) {
    await fetch('/memos/' + event.target.dataset.teckyid, {
      method: 'DELETE'
    })
  
    loadMemos();
  } else if (event.target.matches('.edit-button')) {
    const content = event.target.parentNode.querySelector('.content')
    content.innerHTML = `<textarea>${content.innerHTML}</textarea>`

    const image = document.createElement('input')
    image.type = 'file'
    content.parentNode.appendChild(image)
    
    const submit = document.createElement('input')
    submit.type = 'submit'
    submit.value = '更新'
    content.parentNode.appendChild(submit)
    
    const textarea = content.querySelector('textarea')
    textarea.focus()
    submit.addEventListener('click', async () => {
      const formData = new FormData()
      formData.append('content', textarea.value)
      formData.append('image', image.files[0])

      await fetch('/memos/' + event.target.dataset.teckyid, {
        method: 'PUT',
        body: formData
      })
      
      // await fetch('/memos/' + event.target.dataset.teckyid, {
      //   method: 'PUT',
      //   headers: {
      //     'Content-Type': 'application/json; charset=utf-8'
      //   },
      //   body: JSON.stringify({
      //     content: textarea.value
      //   })
      // })
      loadMemos();
    })
  }
})

loadMemos();

// formData - 可 send file, 後邊用 multer 接
// urlencoded - 不可 send file, 後邊用 bodyParser

const newForm = document.querySelector('#input-new-memo-form')
newForm.addEventListener('submit', async (event) => {
  event.preventDefault();

  const formData = new FormData(newForm)

  // POST /memos
  // Body: formData
  await fetch('/memos', {
    method: 'POST',
    body: formData
  })

  loadMemos();
})

const loginForm = document.querySelector('#login-form')
loginForm.addEventListener('submit', async (event) => {
  event.preventDefault();

  const res = await fetch('/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      username: loginForm.username.value,
      password: document.querySelector('#login-form [name=password]').value
    })
  })
  const json = await res.json();
  if (json.result == 'ok') {
    isAdmin = true;
    loadMemos();
  } else {
    alert('唔識你喎～')
  }
})

async function getLogin() {
  const res = await fetch('/currentUser');
  const json = await res.json();

  if (json.result == 'ok') {
    isAdmin = true;
    loadMemos();
  }
}

getLogin();