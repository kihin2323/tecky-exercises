import React from "react";
import logo from "./logo.svg";
import "./App.css";
import DialogModal from "./components/DialogModal";

function App() {
	return (
		<div className="App">

			<DialogModal></DialogModal>
		</div>
	);
}

export default App;
