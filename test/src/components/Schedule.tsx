import React from "react";
import {
	ScheduleComponent,
	Day,
	Week,
	WorkWeek,
	Month,
	Agenda,
	Inject,
	ViewDirective,
	ViewsDirective,
} from "@syncfusion/ej2-react-schedule";

function Schedule() {
	return (
		<div>
			<ScheduleComponent
				width="100%"
				selectedDate={new Date()}
			>
				<ViewsDirective>
					{/* <ViewDirective
						option="WorkWeek"
						startHour="10:00"
						endHour="18:00"
					/> */}
					<ViewDirective
						option="Week"
						// startHour="10:00"
						// endHour="15:00"
					/>
					{/* <ViewDirective option="Month" showWeekend={false} /> */}
				</ViewsDirective>
				<Inject services={[Week]} />
			</ScheduleComponent>
		</div>
	);
}

export default Schedule;
