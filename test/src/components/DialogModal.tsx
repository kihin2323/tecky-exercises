import { Button, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React from "react";
import Schedule from "./Schedule";

const DialogModal: React.FC = () => {

	const [open, setOpen] = React.useState(false)

	const handleClickOpen = () => {
		setOpen(true);
	};
	const handleClose = () => {
		setOpen(false);
	}

	return (
		<div>
			<Button
				variant="outlined"
				color="primary"
				onClick={handleClickOpen}
			>
				Schedule
			</Button>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="form-dialog-title"
				maxWidth= "lg"
			>
				<DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
				<DialogContent>
					{/* <DialogContentText>
						To subscribe to this website, please enter your email
						address here. We will send updates occasionally.
					</DialogContentText>
					<TextField
						autoFocus
						margin="dense"
						id="name"
						label="Email Address"
						type="email"
						fullWidth
					/> */}
					<Schedule />
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color="primary">
						Save
					</Button>
					<Button onClick={handleClose} color="primary">
						Canel
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
};

export default DialogModal;
