import { Action, bindActionCreators, createStore } from "redux";
import { IRootAction, LOGIN_SUCCESS } from './action';

export interface IRootState {
	isAuthenticated: boolean;
	username: string | null;
}

const initState = {
	isAuthenticated: false,
	username: null,
};

const rootReducer = (state: IRootState = initState, action: IRootAction) => {
	switch (action.type) {
		case LOGIN_SUCCESS:
			return {
				...state,
				isAuthenticated: true,
				username: action.username,
			};
		default:
			return state;
	}
};

export default createStore<IRootState, IRootAction,{} ,{}>(rootReducer)