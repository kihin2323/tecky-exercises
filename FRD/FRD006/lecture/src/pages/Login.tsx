import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../redux/action';

const Login: React.FC = () => {
	const [username, setUsername] = useState("");

	const dispatch = useDispatch()
	const submitHandler = (e: React.FormEvent<HTMLFormElement>)=>{
		e.preventDefault();
		//call server api,the n login success
		const action = loginSuccess(username)
		dispatch(action)
	}
    return (
        <div>
            <h1>This is Login Page</h1>
			<form onSubmit={submitHandler}>
				<input type="text" name="username" value={username} onChange={(e) => setUsername(e.target.value)} required/>
				<button type="submit" value="Submit">Submit</button>
			</form>
        </div>
    )
}

export default Login
