import React from 'react'
import { useSelector } from 'react-redux'
import ProfileContent from '../components/ProfileContent'
import ProfileHeader from '../components/ProfileHeader'
import {IRootState} from "../redux/store"

const Profile: React.FC = () => {
	// const username = useSelector((state: IRootState) => state.username)
    return (
        <div>
            <h1>This is Profile Page</h1>
			<ProfileHeader/>
			<ProfileContent/>
        </div>
    )
}

export default Profile
