import React, { useState } from "react";
import "./App.css";
// import LifeIsNotEasyClass from "./components/LifeIsNotEasyClass";
import LifeIsNotEasyFunc from "./components/LifeIsNotEasyFunc";

function App() {

	const [isDemoDisplay, setIsDemo] = useState(true)

	return (
	<div className="App">
		<h1>Kenneth is Awesome</h1>
		<button onClick={() => {setIsDemo(!isDemoDisplay)}}>Set Display</button>
		{isDemoDisplay && <LifeIsNotEasyFunc/>}
		{/* <LifeIsNotEasyClass/> */}
	</div>
	);
}

export default App;
