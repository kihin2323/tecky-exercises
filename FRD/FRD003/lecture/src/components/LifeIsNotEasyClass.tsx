import React, { Component } from 'react'

interface ILifeIsNotEasyClass{
	num1: number,
	num2: number
}

interface ILifeIsNotEasyProps{}

export default class LifeIsNotEasyClass extends Component<ILifeIsNotEasyProps, ILifeIsNotEasyClass> {

	state = {num1: 100, num2: 200}

	componentDidMount() {
		console.log("this is componentDidMount()")
	}

	componentDidUpdate(preProps: ILifeIsNotEasyProps, preState: ILifeIsNotEasyClass){
		console.log("this is componentDidUpdate()")
		console.log("preProps", preProps)
		console.log("preState", preState);
		if (this.state.num2 >= 205 && preState.num1 === this.state.num1) {
			this.setState({ num1: this.state.num1 + 1 });
		}
	}

	componentWillUnmount() {
		console.log("this is componentWillUnmount()")
	}

	render() {
		console.log("render")
		return (
			<div>
				this is LifeIsNotEasyClass Component
				<h1>Num 1: {this.state.num1}</h1>
				<h1>Num 2: {this.state.num2}</h1>
				<button onClick={() => {this.setState({num1: this.state.num1 + 1})}}>Click Num1</button>
				<button onClick={() => {this.setState({num2: this.state.num2 + 1})}}>Click Num2</button>
			</div>

		)
	}
}
