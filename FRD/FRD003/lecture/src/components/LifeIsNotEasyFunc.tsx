import React, { useEffect, useState } from "react";

const LifeIsNotEasyFunc: React.FC = () => {
	const [num1, setNum1] = useState(100);
	const [num2, setNum2] = useState(200);

	useEffect(()=>{
		console.log("componentDidMount");
		return () => {
			console.log("componentWillUnMount")
		}
	},[]);

	useEffect(()=>{
		console.log("update Number 1")
	},[num1]);

	useEffect(()=>{
		console.log("update Number 2")
		if(num2 >= 205){
			setNum1((num1)=> num1 +1 )
		}
	},[num2]);

	return (
		<div>
			<h1>Num 1: {num1}</h1>
			<h1>Num 2: {num2}</h1>
			<button onClick={() => setNum1(num1 + 1)}>Click Num1</button>
			<button onClick={() => setNum2(num2 + 1)}>Click Num2</button>
		</div>
	);
};

export default LifeIsNotEasyFunc;
