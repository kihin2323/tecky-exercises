import React from "react";
import "./App.css";
import Countdown from "./components/Countdown";

function App() {
	return (
		<div className="App">
			<h1>Jason is handsome</h1>
			<Countdown />
		</div>
	);
}

export default App;
