import React, { useEffect, useRef, useState } from "react";

enum Status {
	START = "START",
	PAUSE = "PAUSE",
	STOP = "STOP",
}

const INTERVAL = 64;
const initValue = 10000;
const Countdown = () => {
	const timeoutRef = useRef<NodeJS.Timeout>();
	const [milisecond, setMilisecond] = useState(initValue);
	const [status, setStatus] = useState(Status.STOP);
	useEffect(() => {
		return () => {
			if (timeoutRef.current) {
				clearInterval(timeoutRef.current);
			}
		};
	}, []);

	useEffect(() => {
		if (milisecond <= 0 && timeoutRef.current) {
			clearInterval(timeoutRef.current);
			setStatus(Status.STOP);
		}
	}, [milisecond]);

	const startHandler = () => {
		setStatus(Status.START);
		timeoutRef.current = setInterval(() => {
			setMilisecond((milisecond) => {
				const newValue = milisecond - INTERVAL;
				return newValue >= 0 ? newValue : 0;
			});
		}, INTERVAL);
	};

	const pauseHandler = () => {
		if (timeoutRef.current) {
			setStatus(Status.PAUSE);
			clearInterval(timeoutRef.current);
		}
	};

	const resetHandler = () => {
		if (timeoutRef.current) {
			setStatus(Status.STOP);
			clearInterval(timeoutRef.current);
			setMilisecond(initValue);
		}
	};

	return (
		<div>
			<h1>{milisecond}</h1>
			<h1>
				{String(Math.floor(milisecond / 1000 / 60)).padStart(2, "0")}:
				{String(Math.floor((milisecond / 1000) % 60)).padStart(2, "0")}:
				{String(Math.floor(milisecond % 1000)).padStart(3, "0")}
			</h1>
			<div>
				{status === Status.STOP && (
					<button onClick={startHandler}>Start</button>
				)}
				{status === Status.START && (
					<button onClick={pauseHandler}>Pause</button>
				)}
				{status === Status.PAUSE && (
					<button onClick={startHandler}>Resume</button>
				)}
				{status !== Status.STOP && (
					<button onClick={resetHandler}>Reset</button>
				)}
			</div>
		</div>
	);
};

export default Countdown;
