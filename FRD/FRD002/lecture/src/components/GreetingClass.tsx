import React, {Component} from  'react';

interface IGreetingClassProps {
    name: string;
    age: number;
}

interface IGreetingClassState{
    score: number;
    age: number;
}
export default class GreetingClass extends Component<IGreetingClassProps, IGreetingClassState,{}> {
    constructor(props: IGreetingClassProps) {
        super(props);
        this.state = {
            score: 10,
            age: props.age,
        };
    }
    render(){
        return(
            <div className="App">
                <h1>Hello, {this.props.name}</h1>
                {this.state.age && <h1>Age: {this.state.age}</h1>}
                <h1>{this.state.score}</h1>
                <button onClick = {() => this.setState({score: this.state.score + 1, age: this.state.age + 1})}>
                    Fun
                </button>
            </div>
        )
    }
}