import React, {useState} from "react";

interface IPerson {
    name: string;
    age?: number;
    score: number
    updateScore: () => void;
}


const Greeting: React.FC<IPerson> = (props) => {
    const name = props.name;
    const score = props.score;
    const [age, setAge] = useState(props.age)
    
    return (
        <div className="App">
            <h1>Hello, {name}</h1>
            {age && <h1>Age: {age}</h1>}
            <h1>score: {score}</h1>
            <button onClick={() => props.updateScore()}>Talk</button>
            <button onClick={() => {
                if(age){
                    setAge(age + 1)
                }
            }}>Age+</button>
        </div>
    )
}
export default Greeting