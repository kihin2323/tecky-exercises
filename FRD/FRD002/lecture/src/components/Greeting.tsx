import React, {useState} from "react";

interface IGreetingProps {
    name: string;
    age: number;
}


const Greeting: React.FC<IGreetingProps> = (props) => {
    const name = props.name;
    const [score, setScore] = useState<number>(10)
    const [age, setAge] = useState<number>(props.age)
    console.log(name + "rendering");
    
    return (
        <div className="App">
            <h1>Hello, {name}</h1>
            {age && <h1>Age: {age}</h1>}
            <h1>score: {score}</h1>
            <button onClick={() => {
                setScore(score + 1)
            }}>Talk</button>
            <button onClick={() => {
                setScore(score - 1)
            }}>Shit</button>
            <button onClick={() => {
                setAge(age + 1)
            }}>Age+</button>
        </div>
    )
}
export default Greeting