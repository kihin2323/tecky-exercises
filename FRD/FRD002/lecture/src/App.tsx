import React, { useState } from "react";
import "./App.css";
// import Greeting from "./components/Greeting"
// import GreetingClass from './components/GreetingClass';
import Person from "./components/Person";
import Form from "./components/Form"
interface Iperson {
  name: string;
  age: number;
  score: number;
}

const initPersonArr = [
  {
    name: "Jason",
    age: 18,
    score: 40,
  },
  {
    name: "Peter",
    age: 18,
    score: 40,
  }
]

function App() {
  // const [personArr, setPersonArr] = useState<Array<Iperson>>();
  const [personArr, setPersonArr] = useState(initPersonArr);
  // const peter = { name: "Peter", age: 18 };
  // const jason = { name: "Jason", age: 18 };

  // const [jasonScore, setJasonScore] = useState(40);
  // const [peterScore, setPeterScore] = useState(20);
  return (
    <div className="App">
      <Form/>
      {/* {personArr.map((person, idx)=>(
        <Person 
            name={person.name}
            age={person.age}
            score={person.score}
            updateScore={()=>{
              const temPersonArr = personArr.slice();
              personArr[idx].score + 1
              for(let i = 0; i < personArr.length; i++){
                if(i ===idx){
                personArr[i].score += 1
                }else {
                  personArr[i].score -= 1
                }
              }
            }}
        />))
      } */}
    </div>
    //   <Person 
    //     name={jason.name}
    //     age={jason.age}
    //     score={jasonScore}
    //     updateScore={()=>{
    //       setJasonScore(jasonScore + 1)
    //       setPeterScore(peterScore - 1)
    //     }}></Person>
        
    //   <Person 
    //     name={peter.name}
    //     age={peter.age}
    //     score={peterScore}
    //     updateScore={()=>{
    //       setJasonScore(jasonScore - 1)
    //       setPeterScore(peterScore + 1)
    //     }}></Person>
  );
}

export default App;
