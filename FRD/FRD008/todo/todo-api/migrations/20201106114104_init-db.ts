import * as Knex from "knex";

const todoTableName = "todo"

export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable(todoTableName, (table) => {
		table.increments();
		table.string("content").notNullable();
		table.boolean("is_complete").notNullable().defaultTo(false);
		table.timestamps(false, true);
	})
}


export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable(todoTableName);
}

