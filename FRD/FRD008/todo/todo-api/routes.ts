import express from "express";
import { todoRoutes } from "./routers/todo";

export const routes = express.Router();
routes.use("/todo", todoRoutes);