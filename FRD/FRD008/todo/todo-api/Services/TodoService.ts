import Knex from "knex";
import tables from "./tables"
export class TodoService{
	constructor(private knex : Knex){

	}
	getAllTodo = async () =>{
		const result = await this.knex(tables.TODO)
		return result;
	}

	createTodo = async (content: string) => {
		const [insertedID] = await this.knex(tables.TODO)
			.insert({ content })
			.returning("id")
		return insertedID;
	}
}