import tables from './tables';
import Knex from 'knex';

const knexFile = require('../knexfile'); // Assuming you test case is inside `services/ folder`
const knex = Knex(knexFile["test"]); // Now the connection is a testing connection.
import { TodoService } from './TodoService';

describe("todoService",()=>{

    let service:TodoService;

    beforeEach(async ()=>{
        service = new TodoService(knex);
        await knex(tables.TODO).del();
        await knex.insert([{
			content: "test 1",
			is_complete: true,
		},{
			content: "test 2",
		}
	])
	.into('todo');
    })

    it("should get all todo",async ()=>{
		const result = await service.getAllTodo()
		expect (result.length).toBe(2);
    });

    afterAll(()=>{
        knex.destroy();
    });
})