import * as Knex from "knex";

const todoTableName = "todo"

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
	await knex(todoTableName).del();
	
	const todoArr = [
		{
			content: "Having lunch with Jason",
			is_complete: true,
		},
		{
			content: "Kenneth is awesome",
		},
	];

	await knex(todoTableName).insert(todoArr);
};
