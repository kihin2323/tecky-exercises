import express from "express";
import { todoController } from "../main";

export const todoRoutes = express.Router();
todoRoutes.get("/", todoController.getAllTodo);
todoRoutes.post("/", todoController.createTodo);