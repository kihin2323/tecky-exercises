import express from 'express';
import {Request,Response} from 'express';
import bodyParser from 'body-parser';

import dotenv from "dotenv";
import Knex from 'knex';
dotenv.config();

const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

const knexConfig = require("./knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

import { TodoService } from "./Services/TodoService";
import { TodoController } from "./controllers/TodoController";
import { routes } from './routes';



const todoService = new TodoService(knex);
export const todoController = new TodoController(todoService);

const API_VERSION = "/api/v1";
app.use(API_VERSION, routes)

app.get("/test", todoController.getAllTodo);
app.post("/test", todoController.createTodo);



app.get('/',function(req:Request,res:Response){
    res.end({msg: "this is Get Test"});
})

app.post('/', function(req:Request, res: Response){
	res.json(req.body)
})
const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});