export interface ITodo{
	id: number;
	content: string;
	is_completed: boolean;
	created_at: Date;
	updated_at: Date;
}