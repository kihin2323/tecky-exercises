import { TodoService } from "../Services/TodoService";
import { Request, Response } from "express"
import { logger } from "../utils/logger";

export class TodoController {
	constructor(private todoService: TodoService){}

	getAllTodo = async (req: Request, res: Response) => {
		try{
			const todoArr = await this.todoService.getAllTodo();
			res.json({todo_arr: todoArr});
		}catch(e){
			logger.error(e.toString());
			res.status(500).json({message: "internal server error"})
		}
	}

	createTodo = async (req: Request, res: Response) => {
		try{
			const content = req.body.content;
			if(!content){
				res.status(400).json({ message: "invalid input" });
				return;
			}
			const id = await this.todoService.createTodo(content)
			res.json({id});
		}catch(e){
			logger.error(e.toString());
			res.status(500).json({message: "internal server error"})
		}
	}
}