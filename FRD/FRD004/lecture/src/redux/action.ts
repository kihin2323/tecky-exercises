export const LOGIN_SUCCESS = "LOGIN_SUCCESS"

interface ILoginSuccess{
	type: typeof LOGIN_SUCCESS,
	username: string
}
export function loginSuccess(username: string): ILoginSuccess{
	return{
		type: LOGIN_SUCCESS,
		username,
	}
}

export type IRootAction = ILoginSuccess