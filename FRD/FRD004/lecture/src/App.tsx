import React, { useState } from "react";
import "./App.css";
import Login from "./pages/Login";
import Navbar from "./components/Navbar";
import Profile from "./pages/Profile";
import { useSelector } from "react-redux";
import { IRootState } from "./redux/store";

function App() {
	const state = useSelector((state: IRootState) => state)
	return (
		<div className="App">
			<Navbar />
			{!state.isAuthenticated && <Login />}
			{state.isAuthenticated && <Profile />}
		</div>
	);
}

export default App;
