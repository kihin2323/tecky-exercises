import React from "react";
import { useSelector } from "react-redux";
import "../css/Navbar.css";
import { IRootState } from "../redux/store";

interface INavbarProps {
	isAuthenticated: boolean;
	username: string;
}

const Navbar: React.FC = () => {
	const state = useSelector((state: IRootState) => state)

	return (
		<nav>
			{state.isAuthenticated && <h3>Hello, {state.username}</h3>}
			{!state.isAuthenticated && <h3>My Demo</h3>}
			<ul className="my-ul">
				<li>login</li>
				<li>Profile</li>
			</ul>
		</nav>
	);
};

export default Navbar;
