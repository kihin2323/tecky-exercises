import React from 'react'
import { useSelector } from 'react-redux'
import { IRootState } from '../redux/store'

const ProfileContent:React.FC = () => {
	const username = useSelector((state: IRootState) => state.username)
	return (
		<div>
			<h1>This is ProfileContent Component</h1>
			<h1>Hello, {username}</h1>
		</div>
	)
}

export default ProfileContent
