//step 1 IAuthState
export interface IAuthState {
    isAuthenticated: boolean;
    msg:string
}
//step 2 
export const initialState = {
    isAuthenticated: (localStorage.getItem('token') !=null),
    msg:""
}

