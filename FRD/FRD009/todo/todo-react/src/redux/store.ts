import {applyMiddleware, combineReducers, compose, createStore} from "redux"
import thunk,{ThunkDispatch as OldThunkDispatch} from 'redux-thunk';
import { IAuthState } from './auth/states';
import { authReducer } from './auth/reducer';
import logger from 'redux-logger';
import { RouterState, connectRouter, routerMiddleware, CallHistoryMethodAction } from 'connected-react-router';
import { createBrowserHistory } from "history"
export const history = createBrowserHistory();


export interface IRootState {
	auth: IAuthState;
}

export type IRootAction = any;

const rootReducers = combineReducers({
	auth: authReducer,
	router: connectRouter(history)
})

declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}
export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore<IRootState,IRootAction,{},{}>(
	rootReducers, 
	composeEnhancers(applyMiddleware(logger)),
	applyMiddleware(thunk),
	applyMiddleware(routerMiddleware(history)),
	)