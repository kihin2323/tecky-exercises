import React from "react";
import logo from "./logo.svg";
import "./App.css";
import {ConnectedRouter} from "connected-react-router"
import {history} from "./redux/store"

function App() {

	return( 
		<ConnectedRouter history={history}>
			<div className="App">
				
			</div>
		</ConnectedRouter>
	)
}

export default App;
