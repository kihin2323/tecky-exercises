import Knex from "knex";
import tables from "./tables";

const knexFile = require("../knexfile");
const knex = Knex(knexFile["test"]);

import { TodoService } from "./TodoService";

describe("TodoService", () => {
    let service: TodoService;

    beforeEach(async () => {
        service = new TodoService(knex);
        await knex(tables.TODO).del();
        await knex
            .insert([
                {
                    content: "Test 1",
                    is_completed: true,
                },
                {
                    content: "Test 2",
                    is_completed: false,
                },
            ])
            .into(tables.TODO);
    });

    it("should get all todo", async () => {
        const result = await service.getAllTodo();
        expect(result.length).toBe(2);
        // const todo = result[0];
        // console.log(typeof todo.created_at);
        // console.log(typeof todo.created_at.getMonth === "function");
    });

    it("should create todo success", async () => {
        const beforeInsetData = await service.getAllTodo();
        const beforeInsetLength = beforeInsetData.length;
        const insertedID = await service.createTodo("this is test");
        expect(insertedID).toBe(beforeInsetData[beforeInsetLength - 1].id + 1);
        const afterInsetLength = (await service.getAllTodo()).length;
        expect(beforeInsetLength + 1).toBe(afterInsetLength);
    });

    afterAll(() => {
        knex.destroy();
    });
});
