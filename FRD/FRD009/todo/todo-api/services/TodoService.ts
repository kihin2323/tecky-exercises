import Knex from "knex";
import tables from "./tables";

export class TodoService {
    constructor(private knex: Knex) {}

    getAllTodo = async (userID: number) => {
        const result = await this.knex(tables.TODO).where("user_id", userID);
        return result;
    };

    createTodo = async (content: string, userID: number) => {
        const [insertedID] = await this.knex(tables.TODO)
            .insert({ content, "user_id": userID })
            .returning("id");
        return insertedID;
    };
}
