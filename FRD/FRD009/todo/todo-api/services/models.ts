export interface ITodo {
    id: number;
    content: string;
	is_completed: boolean;
	user_id: number;
    created_at: Date;
    updated_at: Date;
}
export interface IUser {
    id: number;
    username: string;
    password: boolean;
    created_at: Date;
    updated_at: Date;
}

export interface User{
    id:number
    username:string
    password:string
}

declare global{
    namespace Express{
        interface Request{
            user?: User
        }
    }
}
