# Todo App

&nbsp;

## Git Ignore

-   [x] create `.gitignore`

```Text
node_modules/
.DS_Store
yarn-debug.log*
yarn-error.log*
.env
```

&nbsp;

## Prettier Setup

-   [x] create and config `.prettierrc` in project root directory

```Json
{
    "trailingComma": "es5",
    "tabWidth": 4,
    "semi": true,
    "singleQuote": false
}
```

&nbsp;

## TS Project Template

-   [x] yarn init

-   [x] install packages for TS project template **(WSP001)**

```bash
yarn add ts-node typescript @types/node
```

-   [x] create and config `tsconfig.json` **(WSP001)**

```JSON
{
    "compilerOptions": {
        "module": "commonjs",
        "target": "es5",
        "lib": ["es6", "dom"],
        "sourceMap": true,
        "allowJs": true,
        "jsx": "react",
        "esModuleInterop":true,
        "moduleResolution": "node",
        "noImplicitReturns": true,
        "noImplicitThis": true,
        "noImplicitAny": true,
        "strictNullChecks": true,
        "suppressImplicitAnyIndexErrors": true,
        "noUnusedLocals": true
    },
    "exclude": [
        "node_modules",
        "build",
        "scripts",
        "index.js"
    ]
}
```

-   [x] create and config `index.js` **(WSP001)**

```Javascript
require('ts-node/register');
require('./main');
```

-   [x] create and config `main.ts` **(WSP001)**

```Typescript
console.log("jason is handsome");
```

-   [x] (Optional) set start command in `package.json`

```JSON
{
    ...,
    "scripts": {
        "start": "node ."
    },
    ...
}
```

-   [x] test typescript project template

```Bash
node .
ts-node main.ts
ts-node-dev main.ts
yarn start
```

&nbsp;

## Express

-   [x] install packages for express **(WSP006, WSP007)**

```Bash
yarn add express @types/express
yarn add body-parser @types/body-parser
```

-   [x] config `main.ts` for express template

```Typescript
import express from 'express';
import bodyParser from 'body-parser';
import {Request,Response} from 'express';

const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.get('/',function(req:Request,res:Response){
    res.json({ msg: "This is GET Test" });
})

app.post('/',function(req:Request,res:Response){
    res.json(req.body)
})

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});
```

&nbsp;

## Setup Postman

-   [x] create Collection

-   [x] create Folders for Routers. Create "Dummy Test" folder

-   [x] create Env

-   [x] config Env (add `host` variable)

-   [x] add "GET Test", "POST Test" requests in "Dummy Test" folder

-   [x] config "GET Test", "POST Test" requests and test

&nbsp;

## Jest Test Setup

-   [x] install packages for jest test. we will see `jest.config.js` after init config **(BAD001)**

```Bash
yarn add --dev jest
yarn add --dev typescript ts-jest @types/jest
yarn ts-jest config:init
```

-   [x] create `main.test.ts` and config dummy test case

```Typescript
it("this is dummy test case", () => {
    expect(1+1).toBe(2);
});
```

-   [x] test dummy test case

```Bash
yarn jest

yarn test
```

**Result:**

```Bash
 PASS  ./main.test.ts (5.079 s)
  ✓ this is dummy test case (5 ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        5.213 s
Ran all test suites.
✨  Done in 7.47s.
```

&nbsp;

## Knex Setup

-   [x] install packages for Knex **(BAD003)**

```Bash
yarn add knex @types/knex pg @types/pg
```

-   [x] init config file `knexfile.ts` for Knex **(BAD003)**

```Bash
yarn knex init -x ts
```

-   [x] config `knexfile.ts`

```Typescript
module.exports = {

  development: {
    client: "postgresql",
    connection: {
      database: XXXX,
      user: XXXX,
      password: XXXX
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },
  ...
}
```

-   [x] create database

```Bash
createdb c11_todo_demo
```

-   [x] install `dotenv` package **(WSP010)**

```Bash
yarn add dotenv
```

-   [x] config `.env`, `.env.sample`, `main.ts` and `knexfile.ts`

In `.env`:

```Text
DB_NAME=c11_todo_demo
DB_USERNAME=jason
DB_PASSWORD=jason
```

In `.env.sample`:

```Text
DB_NAME=c11_todo_demo
DB_USERNAME=
DB_PASSWORD=
```

In `main.ts`:

```Typescript
import dotenv from 'dotenv';
dotenv.config();
```

In `knexfile.ts`:

```Typescript
import * as dotenv from 'dotenv';
dotenv.config();

...
{
    database: process.env.DB_NAME,
    user:     process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
}
...
```

&nbsp;

## Migration and Seed

-   [x] create `init-db` migration

```Bash
yarn knex migrate:make -x ts init-db
```

**Remark:**

default env is "development"

```Bash
yarn run v1.22.4
$ /Users/kwanli/project/git/tecky/tecky-exercises-solutions-c11/FRD008/todo/todo-api/node_modules/.bin/knex migrate:make -x ts init-db
Requiring external module ts-node/register
Using environment: development
Using environment: development
Using environment: development
Created Migration: /Users/kwanli/project/git/tecky/tecky-exercises-solutions-c11/FRD008/todo/todo-api/migrations/20201106114109_init-db.ts
✨  Done in 2.36s.
```

-   [x] config migration file **(BAD003)**

```Typescript
import * as Knex from 'knex';

const todoTableName = 'todo';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(todoTableName, (table) => {
        table.increments();
        table.string('content').notNullable().comment('This is Todo Content');
        table.boolean('is_completed').notNullable().defaultTo(false);
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(todoTableName);
}
```

-   [x] migrate **(BAD003)**

```Bash
yarn knex migrate:latest
```

connect db -> create tables

-   [x] check database

-   [x] create seed file **(BAD003)**

```Bash
yarn knex seed:make -x ts init-data
```

-   [x] config seed file

```Typescript
import * as Knex from "knex";

const todoTableName = "todo";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(todoTableName).del();

    const todoArr = [
        {
            content: "Having lunch with Jason",
            is_completed: true,
        },
        {
            content: "Having dinner with Jason",
        },
    ];

    // Inserts seed entries
    await knex(todoTableName).insert(todoArr);
}
```

-   [x] run seed **(BAD003)**

```Bash
yarn knex seed:run
```

&nbsp;

## Services

-   [x] create "service" folder

```Bash
mkdir service
```

-   [x] create `TodoService.ts` in service folder

```Bash
touch TodoService.ts
```

-   [x] config `TodoService.ts`

-   [x] create test case (`TodoService.test.ts` in services folder) for `TodoService.ts` **(BAD005)**

```Bash
touch services/TodoService.test.ts
```

&nbsp;

### Setup Testing (BAD005)

-   [x] add test env in `knexfile.ts`

```Typescript
{
    ...,
    test:{
        client: 'postgresql',
        connection: {
            database: process.env.TEST_DB_NAME,
            user:     process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    },
    staging: {
        //...
    },
    ...
}
```

-   [x] config `.env`, `.env.sample`

In `.env`:

```Text
TEST_DB_NAME=c11_todo_demo_test
```

In `.env.sample`:

```Text
TEST_DB_NAME=c11_todo_demo_test
```

-   [x] create testing db

```Bash
createdb c11_todo_demo_test
```

-   [x] migrate for testing db

```Bash
yarn knex migrate:latest --env test
```

-   [x] config `TodoService.ts`

```Typescript
import Knex from "knex";
import tables from "./tables";

export class TodoService {
    constructor(private knex: Knex) {}

    getAllTodo = async () => {
        const result = await this.knex(tables.TODO);
        return result;
    };

    createTodo = async (content: string) => {
        const [insertedID] = await this.knex(tables.TODO)
            .insert({ content })
            .returning("id");
        return insertedID;
    };
}
```

-   [x] config `TodoService.test.ts`

&nbsp;

## Logger

-   [x] install packages for logger

```Bash
yarn add winston
```

## Controllers

-   [x] create "controllers" folder

```Bash
mkdir controllers
```

-   [x] create `TodoController.ts` in controllers folder

```Bash
touch controllers/TodoController.ts
```

-   [x] config `TodoController.ts`

-   [x] test controller with Postman

In `main.ts`:

```Typescript
...
const knexConfig = require("./knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

import { TodoService } from "./services/TodoService";
import { TodoController } from "./controllers/TodoController";

const todoService = new TodoService(knex);
const todoController = new TodoController(todoService);

app.get("/test", todoController.getAllTodo);
app.post("/test", todoController.createTodo);
...
```

-   [x] test controller with Jest. Create `TodoController.test.ts`

&nbsp;

## Routes

-   [x] create and config `routes.ts`

```Typescript
import express from "express";
import { todoController } from "./main";

export const routes = express.Router();
routes.get("/", todoController.getAllTodo);
routes.post("/", todoController.createTodo);
```

-   [x] modify `main.ts`

```Typescript
...
const todoService = new TodoService(knex);
export const todoController = new TodoController(todoService);

import { routes } from "./routes";

const API_VERSION = "/api/v1";
app.use(API_VERSION, routes);
...
```

-   [x] (Optional) create routers folder

```Bash
mkdir routers
```

-   [x] create and config `todo.ts`

```Bash
touch routers/todo.ts
```

In `routers/todo.ts`

```Typescript
import express from "express";
import { todoController } from "../main";

export const todoRoutes = express.Router();
todoRoutes.get("/", todoController.getAllTodo);
todoRoutes.post("/", todoController.createTodo);
```

-   [x] modify `routes.ts`

```Typescript
import express from "express";
import { todoRoutes } from "./routers/todo";

export const routes = express.Router();
routes.use("/todo", todoRoutes);
```

&nbsp;
