import { TodoController } from "./TodoController";
import { TodoService } from "../services/TodoService";
import { Request, Response } from "express";
import Knex from "knex";

jest.mock("express");

describe("TodoController", () => {
    let controller: TodoController;
    let service: TodoService;
    let resJson: jest.SpyInstance;
    let req: Request;
    let res: Response;
    // const todoData = [
    //     {
    //         id: 1,
    //         content: "having lunch with Jason",
    //         is_completed: true,
    //     },
    // ];

    beforeEach(function () {
        service = new TodoService({} as Knex);
        jest.spyOn(service, "getAllTodo").mockImplementation(() =>
            Promise.resolve([
                {
                    id: 1,
                    content: "having lunch with Jason",
                    is_completed: true,
                },
            ])
        );

        controller = new TodoController(service);
        req = ({} as any) as Request;
        res = ({
            json: () => {},
        } as any) as Response;
        resJson = jest.spyOn(res, "json");
    });

    it("should handle get method correctly", async () => {
        await controller.getAllTodo(req, res);
        expect(service.getAllTodo).toBeCalledTimes(1);
        expect(resJson).toBeCalledWith({
            todo_arr: [
                {
                    id: 1,
                    content: "having lunch with Jason",
                    is_completed: true,
                },
            ],
        });
    });
});
