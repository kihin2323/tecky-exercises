import * as Knex from "knex";

const todoTableName = "todo";
const userTableName = "users";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(todoTableName).del();
	const jason : {id:number} = await knex(userTableName).where("username", "jason").first();
	if(!jason){
		throw new Error("User cannot be found!")
	}

    const todoArr = [
        {
            content: "Having lunch with Jason",
			is_completed: true,
			user_id: jason.id
        },
        {
			content: "Having dinner with Jason",
			user_id: jason.id
        },
    ];

    // Inserts seed entries
    await knex(todoTableName).insert(todoArr);
}
