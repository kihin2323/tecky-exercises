import * as Knex from "knex";
import { hashPassword } from "../hash";

const userTableName = "users";


export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(userTableName).del();

    const password = await hashPassword("1234")

    // Inserts seed entries
    await knex(userTableName).insert([
		{ username: "jason", password: password },
		{ username: "peter", password: password },
	]);
}
