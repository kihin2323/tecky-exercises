import express from "express";
import { todoController } from "../main";
// import { dummyRoutes } from "./dummy";

export const todoRoutes = express.Router();

// todoRoutes.use("/dummy", dummyRoutes);
todoRoutes.get("/", todoController.getAllTodo);
todoRoutes.post("/", todoController.createTodo);

