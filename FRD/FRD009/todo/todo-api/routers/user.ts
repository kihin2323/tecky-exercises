import express from "express";
import { userController } from "../main";
// import { dummyRoutes } from "./dummy";

export const userRoutes = express.Router();

// todoRoutes.use("/dummy", dummyRoutes);
userRoutes.get("/user", userController.post);
