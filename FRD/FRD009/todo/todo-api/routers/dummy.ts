import express, { Request, Response } from "express";

export const dummyRoutes = express.Router();

dummyRoutes.get("/", (req: Request, res: Response) => {
    res.json({ msg: "This is dummy get" });
});

dummyRoutes.post("/", (req: Request, res: Response) => {
    res.json({ msg: "This is dummy post" });
});
