import express from "express";
import bodyParser from "body-parser";

import Knex from "knex";

import dotenv from "dotenv";
dotenv.config();

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const knexConfig = require("./knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

import { TodoService } from "./services/TodoService";
import { TodoController } from "./controllers/TodoController";

import { UserController } from './controllers/UserController';
import { UserService } from './services/UserService';
import { createIsLoggedIn } from "./guard"

const userService = new UserService(knex);
export const isLoggedIn = createIsLoggedIn(userService);
export const userController = new UserController(userService);

const todoService = new TodoService(knex);
export const todoController = new TodoController(todoService);


import { routes } from "./routes";
const API_VERSION = "/api/v1";
app.use(API_VERSION, routes);

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});
