import * as Knex from "knex";

const todoTableName = "todo";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(todoTableName, (table) => {
        table.increments();
        table.string("content").notNullable().comment("This is Todo Content");
        table.boolean("is_completed").notNullable().defaultTo(false);
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(todoTableName);
}
