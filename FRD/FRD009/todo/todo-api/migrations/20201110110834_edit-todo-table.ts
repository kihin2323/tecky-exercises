import * as Knex from "knex";

const todoTableName = "todo";

const userTable = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.table(todoTableName, (table) => {
		table.integer("user_id").notNullable().unsigned();
		table.foreign("user_id").references(`${userTable}.id`)
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(todoTableName);
}