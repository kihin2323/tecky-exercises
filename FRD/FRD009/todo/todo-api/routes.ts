import express from "express";
import { todoRoutes } from "./routers/todo";
import { userRoutes } from "./routers/user"
import { isLoggedIn } from './main';

export const routes = express.Router();
routes.use("/todo", isLoggedIn, todoRoutes);
routes.use("/user", userRoutes)