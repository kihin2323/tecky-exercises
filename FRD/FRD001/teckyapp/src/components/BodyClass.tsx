import React from 'react';
import "../css/BodyClass.css"
class BodyClass extends React.Component{
    render(){
        return (
            <React.Fragment>
            <body>
                <div>This is a simple website made without React. Try to convert this into React enabled.</div>
                <ol>
                    <li>First, you need to use <span className="black-text">create-react-app</span></li>
                    <li>Second, you need to run <span className="black-text"> npm start</span></li>
                </ol>
            </body>
            <img src="../logo.png"></img>
            </React.Fragment>

        )
    }
}

export default BodyClass;