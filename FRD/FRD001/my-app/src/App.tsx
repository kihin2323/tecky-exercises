import React from "react";
import NumList from "./components/NumList"
import NumListClass from "./components/NumListClass"
import "./App.css";

function sayMyName() {
  return "Hello, Jason";
}
function App() {


  const person: any = null;
  return (
    <div className="App">
      {person ? <h1>Hello, {person.name}</h1>: <h1>Hello, Guest</h1>}
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Kenneth Gor
        </a>
      </header> */}
      {/* <ul>
        {[
        <li>1</li>,
        <li>2</li>,
        <li>3</li>,
        <li>4</li>,
        ]}
      </ul> */}
      <NumList/>
      <NumListClass/>
    </div>
  );
}

export default App;
