import React from "react"
import style from "../css/NumList.module.css"
const NumList: React.FC = () =>{
    const arr: Array<number> = [1, 2 ,3, 4]
    return (
        <React.Fragment>
            {/* <div className="NumList__container"> */}
            <div className={style.container}>
                <h1>This is NumList Component</h1>
            </div>
            <div>
                <h1>This is NumList2 Component</h1>
            </div>
            <ul>{arr.length > 0 && 
        arr.map((num, index)=>(
            <li key = {`num_${index}`}>{num}</li>
            ))}
        </ul>
        </React.Fragment>
    )
}
export default NumList
