import React from "react";

class NumListClass extends React.Component{
    render(){
        const arr: Array<number> = [1, 2 ,3, 4]
        return (
            <React.Fragment>
                <div>
                    <h1>This is NumList Component</h1>
                </div>
                <div>
                    <h1>This is NumList3 Component</h1>
                </div>
                <ul>{arr.length > 0 && 
                    arr.map((num, index)=>(
                        <li key = {`num_${index}`}>{num}</li>
                ))}
                </ul>
            </React.Fragment>
        )
    }
}

export default NumListClass