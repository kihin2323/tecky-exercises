// Unit length
const unitLength = 20;
const boxColor = 150;
const strokeColor = 50;
let columns; /* To be determined by window width*/
let rows; /* To be determined by window height */
let currentBoard;
let nextBoard;
let buttonKey = 0;
let pattern;
/**
 * Setup function only run once in the entire program
 */
function setup() {
    const canvas = createCanvas(windowWidth - 30, windowHeight - 350);

    canvas.parent(document.querySelector('#canvas'));

    // Calculate columns and rows
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    // console.log(width, windowWidth)
    // console.log(columns)
    currentBoard = new Array(columns);
    nextBoard = new Array(columns);
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = new Array(rows);
        nextBoard[i] = new Array(rows);
    }
    frameRate(3);
    init();
}

function frame() {
    frameRate(parseInt(document.getElementById("frameRateNum").value))
}

/**
 * Initialize/reset the board state
 */
function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
    buttonKey = 0;
    document.querySelector('.box').innerHTML = "";
}

/**
 * Draw to the canvas base on the current board
 */
function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {
                fill(color(255, 204, 0));
            } else {
                fill(255);
                let neighbors = countNeighbors(i, j);
                if (neighbors > 2) {
                    fill(color('#EE1178'));
                } else if (neighbors > 1) {
                    fill(color('#1178EE'));
                } else if (neighbors > 0) {
                    fill(color('#78EE11'));
                }
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function countNeighbors(x, y) {
    let neighbors = 0
    if (buttonKey == 0) {
        return neighbors;
    } else if (buttonKey % 2 !== 0) {
        for (let i of [-1, 0, 1]) {
            for (let j of [-1, 0, 1]) {
                if (i === j && j === 0) {
                    continue;
                }
                neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
            }
        }
        return neighbors;
    }
}

/**
 * When mouse is dragged
 */
function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }

    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);

    currentBoard[x][y] = 1;
    fill(color(255, 204, 0));
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);

    if (pattern == 1) {
        Block(x, y);
    } else if (pattern == 2) {
        Blinker(x, y)
    } else if (pattern == 3) {
        Glider(x, y)
    } else if (pattern == 4) {
        Boat(x, y)
    } else if (pattern == 5) {
        Tub(x, y)
    } else if (pattern == 6) {
        origin(x, y)
    }
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    mouseDragged();
}

/**
 * When mouse is released
 */
function mouseReleased() {
    loop();
}

function keyPressed() {
    if (keyCode === 83) {
        noLoop();
    } else if (keyCode === 67) {
        loop();
    }
}

/**
 * Generate next generation based on the current board 
 */
function generate() {

    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {


            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i === j && j === 0) {
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }
            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < 2) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > 3) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == 3) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

function Block(x, y) {
    // const x = 15;
    // const y = 15;
    for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
            if (
                (i == 1 && j == 1) ||
                // (i == -1 && j == 0) || 
                // (i == -1 && j == 1) || 
                // (i == 0 && j == 1) || 
                (i == 0 && j == 0) ||
                // (i == 0 && j == 1) ||
                // (i == 1 && j == -1) || 
                (i == 1 && j == 0)
                // (i == 1 && j == 1)
            ) {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 1
            } else {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 0
            }
        }
    }
}

function Blinker(x, y) {
    // const x = 5;
    // const y = 5;
    for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
            if (
                // (i == -1 && j == -1) || 
                // (i == -1 && j == 0) || 
                // (i == -1 && j == 1) || 
                (i == 0 && j == -1) ||
                (i == 0 && j == 0) ||
                (i == 0 && j == 1)
                // (i == 1 && j == -1) || 
                // (i == 1 && j == 0)  || 
                // (i == 1 && j == 1)
            ) {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 1
            } else {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 0
            }
        }
    }
}

function Glider(x, y) {
    // const x = 5;
    // const y = 20;
    for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
            if (
                // (i == -1 && j == -1) || 
                // (i == -1 && j == 0) || 
                (i == -1 && j == 1) ||
                (i == 0 && j == -1) ||
                // (i == 0 && j == 0) || 
                (i == 0 && j == 1) ||
                // (i == 1 && j == -1) || 
                (i == 1 && j == 0) ||
                (i == 1 && j == 1)
            ) {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 1
            } else {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 0
            }
        }
    }
}

function Boat(x, y) {
    // const x = 5;
    // const y = 15;
    for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
            if (
                (i == -1 && j == -1) ||
                (i == -1 && j == 0) ||
                // (i == -1 && j == 1) || 
                (i == 0 && j == -1) ||
                // // (i == 0 && j == 0) || 
                (i == 0 && j == 1) ||
                // // (i == 1 && j == -1) || 
                (i == 1 && j == 0)
                // (i == 1 && j == 1)
            ) {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 1
            } else {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 0
            }
        }
    }
}

function Tub(x, y) {
    // const x = 15;
    // const y = 5;
    for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
            if (
                // (i == -1 && j == -1) || 
                (i == -1 && j == 0) ||
                // (i == -1 && j == 1) || 
                (i == 0 && j == -1) ||
                // (i == 0 && j == 0) || 
                (i == 0 && j == 1) ||
                // (i == 1 && j == -1) || 
                (i == 1 && j == 0)
                // (i == 1 && j == 1)
            ) {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 1
            } else {
                currentBoard[(x + i + columns) % columns][(y + j + rows) % rows] = 0
            }
        }
    }
}

function origin(x, y) {
    currentBoard[x][y] = 1;
    fill(color(255, 204, 0));
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

document.querySelector('#play-game')
    .addEventListener('click', function () {
        loop();
    })


document.querySelector('#pause-game')
    .addEventListener('click', function () {
        noLoop();
    })


let clearColor = document.querySelector('#clear-color')
clearColor.addEventListener('click', function () {
    buttonKey++;
    function explain() {
        if (buttonKey % 2 !== 0) {
            document.querySelector('.box').innerHTML = "Pink = 3 </br> Blue = 2 </br> Green = 1";
        } else {
            document.querySelector('.box').innerHTML = "";
        }
    }
    explain();
});

document.querySelector('#reset-game')
    .addEventListener('click', function () {
        init();
    });


document.querySelector('#block')
    .addEventListener('click', function () {
        pattern = 1
    });
document.querySelector('#blinker')
    .addEventListener('click', function () {
        pattern = 2
    });

document.querySelector('#glider')
    .addEventListener('click', function () {
        pattern = 3
    });

document.querySelector('#boat')
    .addEventListener('click', function () {
        pattern = 4
    });

document.querySelector('#tub')
    .addEventListener('click', function () {
        pattern = 5
    });

document.querySelector('#origin')
    .addEventListener('click', function () {
        pattern = 6
    });