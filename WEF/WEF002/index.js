const peter = {
    name: "Peter",
    age: 50,
    students:[
       { name:"Andy", age:20,exercises:[
          {score: 60, date: new Date("2018-10-01")}
       ]},
       { name:"Bob", age:23,exercises:[
          {score: 76 ,date: new Date("2019-01-05")},
          {score: 55 ,date: new Date("2018-11-05")}
       ]},
       {name: "Charlie", age:25 , exercises:[
           { score: 60 , date: new Date("2019-01-05") }
       ]}
    ]
}

for(let student of peter.students){
    console.log(`Student ${student.name} is ${student.age} year old.`)
}

for(let student of peter.students){
    if(student.exercises) {
        for(let exercise of student.exercises){
          console.log(`Student ${student.name} has an exercises of score: ${exercise.score}`)  
        }
        
    }
}
let andyFound;

for (let student of peter.students){
    if(student.name === 'Andy') {
        andyFound = student;
        break;
    }
}

function findStudent(name){
    let found;
    for (let student of peter.students){
        if(student.name === name) {
            found = student;
            break;
        }
    }
    return found;
}

const exercises= [];

for (let student of peter.students) {
    if(student.exercise) {
        for(let exercise of student.exercises){
            exercises.push(exercise)
        }
    }
}
