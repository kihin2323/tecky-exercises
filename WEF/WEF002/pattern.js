function accumlutor() {
    let sum = 0;
    return function(num) {
        sum = sum + num;
        return sum;
    }
}